\contentsline {chapter}{Acknowledgments}{iii}{section*.1}% 
\contentsline {chapter}{Abstract}{vii}{section*.2}% 
\contentsline {chapter}{Table of Contents}{xi}{section*.3}% 
\contentsline {chapter}{List of Figures}{xiii}{section*.5}% 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Project Goal}{2}{section.1.1}% 
\contentsline {chapter}{\numberline {2}Background and Related Work}{3}{chapter.2}% 
\contentsline {section}{\numberline {2.1}The Whiley Programming Language}{3}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Whiley Architecture}{4}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}Limitations of Whiley}{4}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}Verification Condition Generation}{6}{subsection.2.1.3}% 
\contentsline {subsubsection}{Weakest Precondition Transformer}{6}{section*.8}% 
\contentsline {subsubsection}{Strongest Postcondition Transformer}{6}{section*.9}% 
\contentsline {subsubsection}{Verification of the Sum Example}{7}{section*.10}% 
\contentsline {paragraph}{First Verification Condition:}{8}{section*.12}% 
\contentsline {paragraph}{Second Verification Condition:}{8}{section*.13}% 
\contentsline {paragraph}{Third Verification Condition:}{8}{section*.14}% 
\contentsline {section}{\numberline {2.2}Software Model Checking}{9}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Trace Abstraction Refinement}{9}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Example Trace Abstraction Refinement}{12}{subsection.2.2.2}% 
\contentsline {section}{\numberline {2.3}Ultimate Automizer and ESBMC}{13}{section.2.3}% 
\contentsline {section}{\numberline {2.4}sbt-rats}{14}{section.2.4}% 
\contentsline {chapter}{\numberline {3}Methodology}{17}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Architecture}{18}{section.3.1}% 
\contentsline {section}{\numberline {3.2}How Whiley was Modified to Solve the Indent-Sensitive Parsing Problem}{19}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Translation of Whiley to C}{19}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Limitations of the Translator}{20}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Whiley Constructs}{23}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}C Constructs}{26}{subsection.3.3.3}% 
\contentsline {subsection}{\numberline {3.3.4}Translating the Constructs}{29}{subsection.3.3.4}% 
\contentsline {subsubsection}{Initialisation}{29}{section*.20}% 
\contentsline {subsubsection}{Declaration}{29}{section*.21}% 
\contentsline {subsubsection}{Assign}{29}{section*.22}% 
\contentsline {subsubsection}{Type Declaration}{29}{section*.23}% 
\contentsline {subsubsection}{Constant Declaration}{30}{section*.24}% 
\contentsline {subsubsection}{If Statements}{31}{section*.25}% 
\contentsline {subsubsection}{Switch Statements}{31}{section*.26}% 
\contentsline {subsubsection}{While Statements}{31}{section*.27}% 
\contentsline {subsubsection}{Do .. While Statements}{31}{section*.28}% 
\contentsline {subsubsection}{Function Declaration}{32}{section*.29}% 
\contentsline {subsubsection}{Method Declaration}{33}{section*.30}% 
\contentsline {subsubsection}{Return Type}{33}{section*.31}% 
\contentsline {subsubsection}{Parameters}{34}{section*.32}% 
\contentsline {subsubsection}{Return Statements}{34}{section*.33}% 
\contentsline {subsubsection}{Assert Statements}{34}{section*.34}% 
\contentsline {subsubsection}{Skip, Break, and Continue Statements}{34}{section*.35}% 
\contentsline {subsubsection}{Where Expressions}{34}{section*.36}% 
\contentsline {subsubsection}{Types}{35}{section*.37}% 
\contentsline {subsubsection}{Expressions}{35}{section*.38}% 
\contentsline {subsubsection}{Quantifier Expressions}{36}{section*.39}% 
\contentsline {subsubsection}{LVals}{37}{section*.41}% 
\contentsline {subsubsection}{Identifiers}{37}{section*.42}% 
\contentsline {subsubsection}{Literals}{37}{section*.43}% 
\contentsline {chapter}{\numberline {4}Results}{39}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Absolute Value Function With Error}{40}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Verifier Output}{41}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}Discussion}{42}{subsection.4.1.2}% 
\contentsline {section}{\numberline {4.2}Sum of Non-negative Integers Example}{43}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Output from Verifiers}{44}{subsection.4.2.1}% 
\contentsline {subsection}{\numberline {4.2.2}Discussion}{45}{subsection.4.2.2}% 
\contentsline {section}{\numberline {4.3}Count}{48}{section.4.3}% 
\contentsline {subsection}{\numberline {4.3.1}Output from Verifiers}{49}{subsection.4.3.1}% 
\contentsline {subsection}{\numberline {4.3.2}Discussion}{50}{subsection.4.3.2}% 
\contentsline {section}{\numberline {4.4}Select Positives}{53}{section.4.4}% 
\contentsline {subsection}{\numberline {4.4.1}Output from Verifiers}{55}{subsection.4.4.1}% 
\contentsline {subsection}{\numberline {4.4.2}Discussion}{56}{subsection.4.4.2}% 
\contentsline {section}{\numberline {4.5}Select Positives With Error}{58}{section.4.5}% 
\contentsline {subsection}{\numberline {4.5.1}Output from Verifiers}{60}{subsection.4.5.1}% 
\contentsline {subsection}{\numberline {4.5.2}Discussion}{62}{subsection.4.5.2}% 
\contentsline {section}{\numberline {4.6}Discussion of Results as a Whole}{63}{section.4.6}% 
\contentsline {chapter}{\numberline {5}Conclusion}{65}{chapter.5}% 
\contentsline {subsection}{\numberline {5.0.1}Future Work}{66}{subsection.5.0.1}% 
\contentsline {subsubsection}{Translate Unmodified Whiley Programs}{66}{section*.62}% 
\contentsline {subsubsection}{Translate All Whiley Constructs}{66}{section*.63}% 
\contentsline {subsubsection}{Automate the Verification Process}{67}{section*.64}% 
\contentsline {chapter}{\numberline {6}Abbreviations}{69}{chapter.6}% 
\contentsline {chapter}{\numberline {A}Git Repository}{71}{appendix.A}% 
\contentsline {chapter}{\numberline {B}Hoare Logic}{73}{appendix.B}% 
\contentsline {section}{\numberline {B.1}Hoare Triples}{73}{section.B.1}% 
\contentsline {section}{\numberline {B.2}Rules of Hoare Logic}{74}{section.B.2}% 
\contentsline {subsection}{\numberline {B.2.1}Assign}{74}{subsection.B.2.1}% 
\contentsline {subsection}{\numberline {B.2.2}While}{74}{subsection.B.2.2}% 
\contentsline {subsection}{\numberline {B.2.3}Sequence}{75}{subsection.B.2.3}% 
\contentsline {subsection}{\numberline {B.2.4}Consequence}{76}{subsection.B.2.4}% 
\contentsline {subsection}{\numberline {B.2.5}Condition}{77}{subsection.B.2.5}% 
\contentsline {chapter}{Bibliography}{77}{section*.71}% 
